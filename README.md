# localize.drupal.org

D9+ version of localize.drupal.org site.

## Local development with DDEV

You will need [DDEV](https://ddev.readthedocs.io/en/stable/).

If you haven't installed `mkcert` before, you might need to do it as a one-off.
See information [here](https://ddev.readthedocs.io/en/stable/#linux-and-macos-manual).

### Onboarding

* `git clone git@gitlab.com:drupal-infrastructure/sites/localize.drupal.org.git localizedrupalorg && cd localizedrupalorg`
* `ddev start`
* `ddev composer install`
* If you have [ahoy](https://github.com/ahoy-cli/ahoy#installation): `ahoy recreate`
  * If you do not have nor want to install it, you can run `./scripts/recreate.sh`.

Once you have installed the project, you can use `ddev describe`, which will give you endpoints for:
* Drupal website
* PhpMyAdmin
* Mailhog

### Cron and queues

Queues are decoupled from cron, so we need to set up:
* `drush core:cron`
* `drush l10n_server:scan`
* `drush queue:run l10n_server_parser`
* `drush queue:run l10n_server_packager`

### SSO

To use the SSO authentication with your local copy, you need to add the following to your
`settings.local.php` file:

@todo Add to lastpass?

```
// SSO.
$config['social_auth_keycloak.settings']['server_url'] = 'https://dev-drupal.cloud-iam.com/auth';
$config['social_auth_keycloak.settings']['realm'] = 'main';
$config['social_auth_keycloak.settings']['client_id'] = 'drupalorg-sso';
$config['social_auth_keycloak.settings']['client_secret'] = 'abcde...1234';
```
